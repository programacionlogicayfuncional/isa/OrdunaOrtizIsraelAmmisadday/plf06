(ns plf06.core
  (:gen-class))

(def abc
  (seq "0123456789!\"#$%+'(),&-./;:<=>?@[/]^`{|}
        aábcdeéfghiíjklmnñoópqrstuúüvwxyzAÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZ0123456789!\"#$%+'(),&-./;:<=>?@
        [/]^`{|}"))

(defn rot-13
  [xs]
  (let [f (zipmap abc (drop 13 (take 147 (conj abc))))]
    (apply str (replace f xs))))

(defn -main
  [& args]
  (if (empty? args)
    (println "No se ha ingresado ningun elemento")
      (println (rot-13 (apply str args)))))



 